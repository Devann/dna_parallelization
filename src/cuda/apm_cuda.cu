/**
 * APPROXIMATE PATTERN MATCHING
 *
 * INF560 X2016
 */

#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/time.h>
#include <math.h>

#include "cuda_stuff.cuh"

#define APM_DEBUG 0
#define MAX_BLOCK_SIZE 1024

char *
read_input_file(char *filename, int *size)
{
    char *buf;
    off_t fsize;
    int fd = 0;
    int n_bytes = 1;

    /* Open the text file */
    fd = open(filename, O_RDONLY);
    if (fd == -1)
    {
        fprintf(stderr, "Unable to open the text file <%s>\n", filename);
        return NULL;
    }

    /* Get the number of characters in the textfile */
    fsize = lseek(fd, 0, SEEK_END);
    lseek(fd, 0, SEEK_SET);

#if APM_DEBUG
    printf("File length: %ld\n", fsize);
#endif

    /* Allocate data to copy the target text */
    buf = (char *)malloc(fsize * sizeof(char));
    if (buf == NULL)
    {
        fprintf(stderr, "Unable to allocate %ld byte(s) for main array\n",
                fsize);
        return NULL;
    }

    n_bytes = read(fd, buf, fsize);
    if (n_bytes != fsize)
    {
        fprintf(stderr,
                "Unable to copy %ld byte(s) from text file (%d byte(s) copied)\n",
                fsize, n_bytes);
        return NULL;
    }

#if APM_DEBUG
    printf("Number of read bytes: %d\n", n_bytes);
#endif

    *size = n_bytes;

    close(fd);

    return buf;
}

__global__ void doLevenshtein(int *pattern_size, int *buf_size, int *approx_factor, char *devicePattern, char *deviceBuf, int *deviceResult)
{
    int buf_size_val = *buf_size;
    int pattern_size_val = *pattern_size;
    int approx_factor_val = *approx_factor;
    int idx = blockIdx.x * blockDim.x + threadIdx.x;

    extern __shared__ int shared_columns[];

    if (idx < buf_size_val - pattern_size_val)
    {
        int *column = shared_columns + threadIdx.x * (pattern_size_val + 1);
        int distance = 0;

        // Calculate the Levenshtein distance
        unsigned int x, y, lastdiag, olddiag;

        for (y = 1; y <= pattern_size_val; y++)
        {
            column[y] = y;
        }

#if APM_DEBUG
        if (blockIdx.x == 0 && threadIdx.x == 255)
        {
            printf("In block %d and thread %d, column index is %d\n", blockIdx.x, threadIdx.x, threadIdx.x * (pattern_size_val + 1));
            for (y = 1; y <= pattern_size_val; y++)
            {
                printf("column[%d] = %d\n", y, column[y]);
            }
        }
#endif
        for (x = 1; x <= pattern_size_val; x++)
        {
            column[0] = x;
            lastdiag = x - 1;
            for (y = 1; y <= pattern_size_val; y++)
            {
                olddiag = column[y];

                column[y] = column[y] + 1;

                if (column[y - 1] + 1 < column[y])
                {
                    column[y] = column[y - 1] + 1;
                }

                int sub_cost = 1;
                if (devicePattern[y - 1] == deviceBuf[idx + x - 1])
                {
                    sub_cost = 0;
                }

                if (lastdiag + sub_cost < column[y])
                {
                    column[y] = lastdiag + sub_cost;
                }

                lastdiag = olddiag;
            }
        }
        distance = column[pattern_size_val];
        if (distance <= approx_factor_val)
        {
            deviceResult[idx] = 1;
        }
    }
}

int main(int argc, char **argv)
{
    char **pattern;
    char *filename;
    int approx_factor = 0;
    int nb_patterns = 0;
    int i;
    char *buf;
    struct timeval t1, t2, t1bis;
    double duration, overhead_duration;
    int n_bytes;
    int *n_matches;

    /* Check number of arguments */
    if (argc < 4)
    {
        printf("Usage: %s approximation_factor "
               "dna_database pattern1 pattern2 ...\n",
               argv[0]);
        return 1;
    }

    /* Get the distance factor */
    approx_factor = atoi(argv[1]);

    /* Grab the filename containing the target text */
    filename = argv[2];

    /* Get the number of patterns that the user wants to search for */
    nb_patterns = argc - 3;

    /* Fill the pattern array */
    pattern = (char **)malloc(nb_patterns * sizeof(char *));
    if (pattern == NULL)
    {
        fprintf(stderr,
                "Unable to allocate array of pattern of size %d\n",
                nb_patterns);
        return 1;
    }

    /* Grab the patterns */
    for (i = 0; i < nb_patterns; i++)
    {
        int l;

        l = strlen(argv[i + 3]);
        if (l <= 0)
        {
            fprintf(stderr, "Error while parsing argument %d\n", i + 3);
            return 1;
        }

        pattern[i] = (char *)malloc((l + 1) * sizeof(char));
        if (pattern[i] == NULL)
        {
            fprintf(stderr, "Unable to allocate string of size %d\n", l);
            return 1;
        }

        strncpy(pattern[i], argv[i + 3], (l + 1));
    }

    printf("Approximate Pattern Mathing: "
           "looking for %d pattern(s) in file %s w/ distance of %d\n",
           nb_patterns, filename, approx_factor);

    buf = read_input_file(filename, &n_bytes);

    if (buf == NULL)
    {
        return 1;
    }

    /* Allocate the array of matches */
    n_matches = (int *)malloc(nb_patterns * sizeof(int));
    if (n_matches == NULL)
    {
        fprintf(stderr, "Error: unable to allocate memory for %ldB\n",
                nb_patterns * sizeof(int));
        return 1;
    }

    /*****
     * BEGIN MAIN LOOP
     ******/
    /* Timer start */
    gettimeofday(&t1, NULL);

    // device variables
    char *deviceBuf;
    char *devicePattern;
    int *deviceResult;
    int *deviceBufSize;
    int *devicePatternSize;
    int *deviceApproxFactor;

    for (i = 0; i < nb_patterns; i++)
    {
        int size_pattern = strlen(pattern[i]);
        n_matches[i] = 0;

        /* Allocate the array of results */
        int *hostResult = NULL;
        hostResult = (int *)malloc(n_bytes * sizeof(int));
        memset(hostResult, 0, n_bytes * sizeof(int));

        // CUDA START

        // cuda allocs
        deviceBuf = NULL;
        gpuErrchk(cudaMalloc((void **)&deviceBuf, n_bytes * sizeof(char)));
        gpuErrchk(cudaMemcpy(deviceBuf, buf, n_bytes * sizeof(char), cudaMemcpyHostToDevice));

        devicePattern = NULL;
        gpuErrchk(cudaMalloc((void **)&devicePattern, size_pattern * sizeof(char)));
        gpuErrchk(cudaMemcpy(devicePattern, pattern[i], size_pattern * sizeof(char), cudaMemcpyHostToDevice));

        deviceResult = NULL;
        gpuErrchk(cudaMalloc((void **)&deviceResult, n_bytes * sizeof(int)));
        gpuErrchk(cudaMemcpy(deviceResult, hostResult, n_bytes * sizeof(int), cudaMemcpyHostToDevice));

        deviceBufSize = NULL;
        gpuErrchk(cudaMalloc((void **)&deviceBufSize, sizeof(int)));
        gpuErrchk(cudaMemcpy(deviceBufSize, &n_bytes, sizeof(int), cudaMemcpyHostToDevice));

        devicePatternSize = NULL;
        gpuErrchk(cudaMalloc((void **)&devicePatternSize, sizeof(int)));
        gpuErrchk(cudaMemcpy(devicePatternSize, &size_pattern, sizeof(int), cudaMemcpyHostToDevice));

        deviceApproxFactor = NULL;
        gpuErrchk(cudaMalloc((void **)&deviceApproxFactor, sizeof(int)));
        gpuErrchk(cudaMemcpy(deviceApproxFactor, &approx_factor, sizeof(int), cudaMemcpyHostToDevice));

        // CUDA getting GPU info
        int deviceCount;
        cudaGetDeviceCount(&deviceCount);
        if (deviceCount != 1)
        {
            fprintf(stderr, "No CUDA devices found.\n");
            return 1;
        }
        int device = 0;
        cudaDeviceProp deviceProp;
        cudaGetDeviceProperties(&deviceProp, device);
        size_t maxSharedMemory = deviceProp.sharedMemPerBlock;

        // CUDA setting up blocks and grid
        int blockSize = floor(maxSharedMemory / (sizeof(int) * (size_pattern + 1)));
        if (blockSize > MAX_BLOCK_SIZE)
        {
            blockSize = MAX_BLOCK_SIZE;
        }
        int sharedMemorySize = sizeof(int) * blockSize * (size_pattern + 1);
        int numBlocks = ceil(n_bytes / blockSize) + 1;

        // DEBUG

#if APM_DEBUG
        printf("sizeof(int) = %lu\n", sizeof(int));
        printf("size_pattern = %d, size of a column = %lu, n_bytes = %d\n", size_pattern, (size_pattern + 1) * sizeof(int), n_bytes);
        printf("Max Shared Memory Per Block: %lu bytes\n", maxSharedMemory);
        printf("blockSize = %d, numBlocks = %d, total thread number = %d\n", blockSize, numBlocks, blockSize * numBlocks);

        printf("Device %d: %s\n", device, deviceProp.name);
        printf("  Max Threads per Block: %d\n", deviceProp.maxThreadsPerBlock);
        printf("  Max Grid Dimensions: (%d, %d, %d)\n", deviceProp.maxGridSize[0], deviceProp.maxGridSize[1], deviceProp.maxGridSize[2]);

        int maxBlocksX = deviceProp.maxGridSize[0];
        int maxBlocksY = deviceProp.maxGridSize[1];
        int maxBlocksZ = deviceProp.maxGridSize[2];

        uint64_t maxBlocks = maxBlocksX * maxBlocksY * maxBlocksZ;
        printf("  Max Number of Blocks in Grid: %lu\n", maxBlocks);

        cudaDeviceSynchronize();
#endif

        // cuda kernel
        gettimeofday(&t1bis, NULL);
        cudaGetLastError();
        doLevenshtein<<<numBlocks, blockSize, sharedMemorySize>>>(devicePatternSize, deviceBufSize, deviceApproxFactor, devicePattern, deviceBuf, deviceResult);
        gpuErrchk(cudaPeekAtLastError());
        gpuErrchk(cudaDeviceSynchronize());

        printf("cuda after kernel synchronized\n");
        // get results from device to host
        cudaMemcpy(hostResult, deviceResult, n_bytes * sizeof(int), cudaMemcpyDeviceToHost);

        // Free memory
        gpuErrchk(cudaFree(deviceBuf));
        gpuErrchk(cudaFree(devicePattern));
        gpuErrchk(cudaFree(deviceResult));
        gpuErrchk(cudaFree(deviceBufSize));
        gpuErrchk(cudaFree(devicePatternSize));
        gpuErrchk(cudaFree(deviceApproxFactor));

        // sum up results
        int sum = 0;
        for (int k = 0; k < n_bytes; k++)
        {
            sum += hostResult[k];
        }
        n_matches[i] = sum;
    }

    /* Timer stop */
    gettimeofday(&t2, NULL);

    duration = (t2.tv_sec - t1.tv_sec) + ((t2.tv_usec - t1.tv_usec) / 1e6);

    printf("APM done in %lf s\n", duration);

    overhead_duration = (t1bis.tv_sec - t1.tv_sec) + ((t1bis.tv_usec - t1.tv_usec) / 1e6);

    printf("Overhead duration = %lf s\n", overhead_duration);

    /*****
     * END MAIN LOOP
     ******/

    for (i = 0; i < nb_patterns; i++)
    {
        printf("Number of matches for pattern <%s>: %d\n",
               pattern[i], n_matches[i]);
    }

    return 0;
}