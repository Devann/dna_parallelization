# OpenMP
# ./scripts/openmp/start_openmp.sh 10 "static" 1
# ./scripts/openmp/start_openmp.sh 10 "static" 2
# ./scripts/openmp/start_openmp.sh 10 "static" 3
# ./scripts/openmp/start_openmp.sh 10 "static" 1024
# ./scripts/openmp/start_openmp.sh 10 "dynamic" 1
# ./scripts/openmp/start_openmp.sh 10 "dynamic" 2
# ./scripts/openmp/start_openmp.sh 10 "dynamic" 3
# ./scripts/openmp/start_openmp.sh 10 "dynamic" 1024
# ./scripts/openmp/start_openmp.sh 10 "guided" 1
# ./scripts/openmp/start_openmp.sh 10 "guided" 2
# ./scripts/openmp/start_openmp.sh 10 "guided" 3
# ./scripts/openmp/start_openmp.sh 10 "guided" 1024

# OpenMPI
./scripts/openmpi/start_openmp_mpi.sh 10 6 "static" 1
./scripts/openmpi/start_openmp_mpi.sh 10 6 "static" 2
./scripts/openmpi/start_openmp_mpi.sh 10 6 "static" 3
./scripts/openmpi/start_openmp_mpi.sh 10 6 "static" 1024
./scripts/openmpi/start_openmp_mpi.sh 10 6 "dynamic" 1
./scripts/openmpi/start_openmp_mpi.sh 10 6 "dynamic" 2
./scripts/openmpi/start_openmp_mpi.sh 10 6 "dynamic" 3
./scripts/openmpi/start_openmp_mpi.sh 10 6 "dynamic" 1024
./scripts/openmpi/start_openmp_mpi.sh 10 6 "guided" 1
./scripts/openmpi/start_openmp_mpi.sh 10 6 "guided" 2
./scripts/openmpi/start_openmp_mpi.sh 10 6 "guided" 3
./scripts/openmpi/start_openmp_mpi.sh 10 6 "guided" 1024

# MPI
# ./scripts/mpi/start_mpi.sh 10 4
# ./scripts/mpi/start_mpi.sh 10 6
# ./scripts/mpi/start_mpi.sh 10 8
# ./scripts/mpi/start_mpi.sh 10 10
# ./scripts/mpi/start_mpi.sh 10 16
# ./scripts/mpi/start_mpi.sh 10 24
