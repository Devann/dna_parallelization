# DNA Parallelization

This repository contains a parallelized version of the pattern matching program available here: [apm.tgz](https://www-inf.telecom-sudparis.eu/COURS/CSC5001/Supports/Projet/Projet2023/PatternMatching/apm.tgz).

## Authors

- Malek HAMMOU (malek.hammou@telecom-sudparis.eu)
- Devan PRIGENT (devan.prigent@telecom-sudparis.eu)
- Jules RISSE (jules.risse@telecom-sudparis.eu)
