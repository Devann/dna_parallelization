#!/bin/bash

# ARGUMENTS
if [ $# -ne 2 ]; then
  echo "Usage: start_mpi.sh <nb_executions> <nprocs>"
  exit 1
else
  nb_executions=$1
  nprocs=$2
fi

# VARIABLES
nhosts=$(wc -l "./src/machines" | awk '{print $1}')
type_parallelization="mpi"
program="apm_mpi"
pattern_path="./Report/data/${type_parallelization}_${nprocs}_procs"

# SETTING
if [ -d "./scripts/logs" ]; then
    rm -r "./scripts/logs"
fi
mkdir -p "./scripts/logs"

# COMPILATION
mpicc -o "$program" "./src/${program}.c" -lm

# SCRIPTS
echo "----------------------------"
echo "[INFO] START PERFORMANCE MEASUREMENT"
echo "[INFO] TYPE OF PARALLELIZATION = ${type_parallelization} ; NUMBER OF PROCS = ${nprocs}"

## GENERATE DATA FILES WITH SEVERAL EXECUTIONS
echo "----------------------------"
echo "[INFO] START COMPUTING DATA"
for ((i=1; i<$nb_executions+1; i++))
do
    output_file="${pattern_path}_v${i}.txt"
    ./scripts/mpi/generate_data_mpi.sh "$program" "$output_file" $nprocs
    echo "[INFO] EXECUTION N°${i} DONE"
done
echo "[INFO] END COMPUTING DATA"

## GATHER ALL DATA IN ONE FILE
echo "----------------------------"
echo "[INFO] START FORMATING DATA"
python3 ./scripts/common/format_data.py "$pattern_path" $nb_executions
echo "[INFO] END FORMATING DATA"

## DISPLAY DATA
echo "----------------------------"
echo "[INFO] START DISPLAYING DATA"
data_file="${pattern_path}_final.txt"
python3 ./scripts/common/display_data.py "$data_file" "$type_parallelization" "$nb_executions" "$nhosts" "$nprocs"
echo "[INFO] END DISPLAYING DATA"

# CLEANING
echo "----------------------------"
rm "$program"
