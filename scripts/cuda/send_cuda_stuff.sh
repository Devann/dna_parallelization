#!/bin/bash

# Send files to gpu computer with ssh
scp ./../src/cuda/apm_cuda.cu risse_ju@tsp:/netfs/ei1821/risse_ju/CSC_HPC/CUDA_Tests/
scp ./../src/cuda/cuda_stuff.cuh risse_ju@tsp:/netfs/ei1821/risse_ju/CSC_HPC/CUDA_Tests/
scp ./../src/cuda/cuda_world.cu risse_ju@tsp:/netfs/ei1821/risse_ju/CSC_HPC/CUDA_Tests/
scp ./cuda_start.sh risse_ju@tsp:/netfs/ei1821/risse_ju/CSC_HPC/CUDA_Tests/


# if dna needed
# scp ../dna/U00096.3.fasta risse_ju@tsp:/netfs/ei1821/risse_ju/CSC_HPC/CUDA_Tests/dna/
# scp ../dna/U00096.3.fasta risse_ju@tsp:/netfs/ei1821/risse_ju/CSC_HPC/CUDA_Tests/dna/

echo "DONE!"