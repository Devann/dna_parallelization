import matplotlib.pyplot as plt

data = {
    "Sequential": 1.42,
    "OpenMp 6": 0.282,
    "OpenMp 12": 0.165,
    "OpenMp 24": 0.0916,
    "MPI 6": 0.245,
    "MPI 24": 0.142,
    "Cuda": 0.142,
}

custom_colors = ['dimgray', 'darkred', 'red', 'firebrick', 'darkgreen', 'green', 'blue']
types = list(data.keys())
temps = list(data.values())

plt.figure(figsize=(10, 6))
bars = plt.bar(types, temps, color=custom_colors)
plt.ylabel('Temps (s)')
plt.title('Execution Time for different parallelizations methods. \n Searching for pattern "AAAAAAA" in E.Coli genome with approximation factor of 1')
plt.xticks(rotation=45)
plt.tight_layout()

# Add labels above each bar
for bar, temp in zip(bars, temps):
    plt.text(bar.get_x() + bar.get_width() / 2, bar.get_height() + 0.01, f'{temp:.3f} s', ha='center')

plt.show()
