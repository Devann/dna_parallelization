import matplotlib.pyplot as plt

data = {
    "Sequential" : 114.071,
    "OpenMp 6" : 20.849,
    "OpenMp 12" : 10.631,
    "OpenMp 24" : 5.98,
    "MPI 6" : 19.173,
    "MPI 24" : 5.27,
    "Cuda" : 0.408

}

custom_colors = ['dimgray', 'darkred', 'red', 'firebrick', 'darkgreen', 'green', 'blue']
types = list(data.keys())
temps = list(data.values())

plt.figure(figsize=(10, 6))
bars = plt.bar(types, temps, color=custom_colors)
plt.ylabel('Temps (s)')
plt.title('Execution Time for different parallelizations methods. \n Searching for exact pattern : "AGCTTTTCATTCTGACTGCAACGGGCAATATGTCTCTGTGTGGATTAAAAAAAGAGTGTC"\n')
plt.xticks(rotation=45)
plt.tight_layout()

# Add labels above each bar
for bar, temp in zip(bars, temps):
    plt.text(bar.get_x() + bar.get_width() / 2, bar.get_height() + 0.01, f'{temp:.3f} s', ha='center')

plt.show()
