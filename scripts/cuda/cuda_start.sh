#!/bin/bash
# this scripts only works on cuda compatible machines

# compile
nvcc -o apm_cuda apm_cuda.cu
echo "COMPIL OK"

# execute
./apm_cuda 1 ./dna/U00096.3.fasta ./dna/line_chrY.fa

# clean
rm ./apm_cuda