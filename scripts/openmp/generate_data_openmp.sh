#!/bin/bash

# ARGUMENTS
if [ $# -lt 2 ]; then
  echo "Usage: generate_data_openmp.sh <program> <result_file>"
  exit 1
else
  program=$1
  result_file=$2
fi

# SETTING
approximation_factor=1
dna_database="./dna/U00096.3.fasta"
pattern_dna="AAAAAAA"
result_folder="./scripts/logs/"
export GOMP_CPU_AFFINITY=$(hwloc-calc --physical-output --intersect PU --no-smt all)

# COMPUTING
nth_max=$(hwloc-calc --number-of core all)

for nb_th in $(seq $nth_max); do
  export OMP_NUM_THREADS=$nb_th
  ./${program} $approximation_factor $dna_database $pattern_dna > "${result_folder}${program}_${nb_th}.log"
done

# DISPLAYING RESULTS
pattern="${result_folder}${program}"
path="${pattern}_1.log"
seq_time=$(grep 'APM done in' $path | awk '{print $4}')
seq_time_float=$(echo "$seq_time" | bc -l)
echo "$program" > $result_file
echo "tempseq $seq_time" >> $result_file 
echo "nbprocs time speedup" >> $result_file
for nb_th in $(seq $nth_max); do
    path="${pattern}_${nb_th}.log"
    th_time=$(grep 'APM done in' ${path}| awk '{print $4}')
    th_time_float=$(echo "$th_time" | bc -l)
    speedup=$(bc -l <<< "$seq_time_float / $th_time_float")
    echo -e "$nb_th, $th_time, $speedup" >> $result_file
done
