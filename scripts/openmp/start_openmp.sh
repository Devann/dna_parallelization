#!/bin/bash

# ARGUMENTS
if [ $# -ne 3 ]; then
  echo "Usage: start_openmp.sh <nb_executions> <scheduling> <chunk_size>"
  exit 1
else
  nb_executions=$1
  scheduling=$2
  chunk_size=$3
fi

# VARIABLES
type_parallelization="openmp"
program="apm_openmp"
nthreads=$(nproc)
ncores=$(echo "$nthreads/2" | bc)
pattern_path="./Report/data/${type_parallelization}_${ncores}_cores_${scheduling}_${chunk_size}_chunk"

# SETTING
if [ -d "./scripts/logs" ]; then
    rm -r "./scripts/logs"
fi
mkdir -p "./scripts/logs"

# COMPILATION
gcc -o "$program" "./src/${program}.c" -fopenmp -DSCHEDULING=$scheduling -DCHUNK_SIZE=$chunk_size

# SCRIPTS
echo "----------------------------"
echo "[INFO] START PERFORMANCE MEASUREMENT"
echo "[INFO] TYPE OF PARALLELIZATION = ${type_parallelization} ; NUMBER OF EXECUTIONS = ${nb_executions} ; SCHEDULING = ${scheduling} ; CHUNK SIZE = ${chunk_size}"

## GENERATE DATA FILES WITH SEVERAL EXECUTIONS
echo "----------------------------"
echo "[INFO] START COMPUTING DATA"
for ((i=1; i<$nb_executions+1; i++))
do
    output_file="${pattern_path}_v${i}.txt"
    ./scripts/openmp/generate_data_openmp.sh "$program" "$output_file"
    echo "[INFO] EXECUTION N°${i} DONE"
done
echo "[INFO] END COMPUTING DATA"

## GATHER ALL DATA IN ONE FILE
echo "----------------------------"
echo "[INFO] START FORMATING DATA"
python3 ./scripts/common/format_data.py "$pattern_path" $nb_executions
echo "[INFO] END FORMATING DATA"

## DISPLAY DATA
echo "----------------------------"
echo "[INFO] START DISPLAYING DATA"
data_file="${pattern_path}_final.txt"
python3 ./scripts/common/display_data.py "$data_file" "$type_parallelization" "$nb_executions" "$ncores" "$scheduling" "$chunk_size"
echo "[INFO] END DISPLAYING DATA"

# CLEANING
echo "----------------------------"
rm "$program"
