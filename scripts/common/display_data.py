# LIBRAIRIES
import sys
import numpy as np
import seaborn as sns
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib as mpl

# ARGUMENTS
if len(sys.argv) > 1:
    data_file = sys.argv[1]
    type_parallelization = sys.argv[2]
    nb_executions = sys.argv[3]

    if type_parallelization == "openmp":
        ncores = sys.argv[4]
        scheduling = sys.argv[5]
        chunk_size = sys.argv[6]
    elif type_parallelization == "mpi":
        nhosts=sys.argv[4]
        nprocs=sys.argv[5]
    elif type_parallelization == "openmpi":
        nhosts=sys.argv[4]
        nprocs=sys.argv[5]
        scheduling = sys.argv[6]
        chunk_size = sys.argv[7]

else:
    print("Usage: script_display.py <data_file> <nb_cores> <type_parallelization> <nb_executions> <scheduling> <chunk_size>")
    exit()

# VARIABLES
xlabel='Number of Cores'
ylabel='Speedup'
output_file = ""

if type_parallelization == "openmp":
    output_file=f'./Report/figures/{type_parallelization}_{ncores}_cores_{scheduling}_{chunk_size}_chunksize_{nb_executions}_executions.png'
    title=f'(OPENMP) Speedup for {scheduling} scheduling with chunk size of {chunk_size} ({nb_executions} executions)'
elif type_parallelization == "mpi":
    output_file=f'./Report/figures/{type_parallelization}_{nprocs}_nprocs_{nb_executions}_executions.png'
    title=f'(MPI) Speedup with {nhosts} different hosts ({nb_executions} executions)'
elif type_parallelization == "openmpi":
    output_file=f'./Report/figures/{type_parallelization}_{nprocs}_nprocs_{scheduling}_{chunk_size}_chunksize_{nb_executions}_executions.png'
    title=f'(OpenMPI) Speedup with {nhosts} different hosts for {scheduling} scheduling with chunk size of {chunk_size}  ({nb_executions} executions)'

# READ DATA
with open(data_file) as f:
    program = f.readline().strip()
    temps_seq = f.readline().split()[1]
    df = pd.read_csv(f, header=0, names=['nbprocs', 'time', 'speedup'])

# DISPLAY DATA
data = df.groupby(["nbprocs"],as_index=False).agg(
    min=pd.NamedAgg(column="speedup", aggfunc="min"),
    max=pd.NamedAgg(column="speedup", aggfunc="max"),
    mean=pd.NamedAgg(column="speedup", aggfunc="mean"))
data.reset_index(inplace=True)
ax = data.plot(x='nbprocs', y='mean', style='-', color='blue', label='Average Speedup', marker='o')
ax.fill_between(x='nbprocs', y1='min', y2='max', data=data,
                color=mpl.colors.to_rgba('red', 0.15))
ax.plot(np.array(data['nbprocs']), np.array(data['nbprocs']), linestyle='--', color='r', label='Ideal Speedup')

plt.xlabel(xlabel)
plt.ylabel(ylabel)
plt.title(title)
plt.grid(True)
plt.legend()
plt.savefig(output_file)
print(f'[INFO] Results saved in {output_file}')
#plt.show()
