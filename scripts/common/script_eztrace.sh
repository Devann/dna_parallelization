#!/bin/bash

# ARGUMENTS
program="apm_openmp"
approximation_factor=1
dna_database="./dna/U00096.3.fasta"
pattern1="./dna/line_chrY.fa"

# SET NTHREADS
nb_threads=$(hwloc-calc --number-of core all)
export OMP_NUM_THREADS=$nb_threads

# COMPILATION
eztrace_cc gcc -o "$program" "./src/${program}.c" -fopenmp

# EZTRACE
eztrace -t openmp -o ./scripts/logs/eztrace_logs/ ./${program} $approximation_factor $dna_database $pattern1

# Vite
vite ./scripts/logs/eztrace_logs/eztrace_log.otf2