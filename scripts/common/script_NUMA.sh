#!/bin/bash

# Compile
gcc -o ./src/apm_openmp ./src/apm_openmp.c -DSCHEDULING="dynamic" -DCHUNK_SIZE=1 -fopenmp
gcc -o ./src/apm_openmp_NUMA ./src/apm_openmp_NUMA.c -DSCHEDULING="dynamic" -DCHUNK_SIZE=1 -fopenmp

echo $PWD

# set core
nth_max=$(hwloc-calc --number-of core all)
export OMP_NUM_THREADS=$nth_max

# Launch
./src/apm_openmp 1 ./dna/U00096.3.fasta ./dna/line_chrY.fa
./src/apm_openmp_NUMA 1 ./dna/U00096.3.fasta ./dna/line_chrY.fa

# clean
rm ./src/apm_openmp
rm ./src/apm_openmp_NUMA