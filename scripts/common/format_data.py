import sys

if len(sys.argv) == 3:
    files_path = sys.argv[1]
    nb_executions = int(sys.argv[2])
else:
    print("Usage: 2_format_data.py <files_path> <nb_executions>")
    exit()
    

def generate_files_list(path, indice):
    files_list = []
    for number in range(1, indice+1):
        files_list.append(path + "_v" + str(number) + ".txt")
    return files_list

def generate_formated_data(files_list, output_file):
    with open(output_file, 'w') as output:
        output.write("apm_openmp\ntempseq 0\nnbprocs time speedup\n")
        for file in files_list:
            print(f"[INFO] Reading file {file}")
            with open(file) as f:
                f.readline()
                f.readline()
                f.readline()
                output.write(f.read())

def start():
    files_list = generate_files_list(files_path,nb_executions)
    output_file = files_path + "_final.txt"
    generate_formated_data(files_list,output_file)
    print("[INFO] DATA GENERATED")

start()